import { constantRoutes } from "@/router";
import Layout from "@/layout";

/**
 * Use meta.role to determine if the current user has permission
 * @param roles
 * @param route
 */
function hasPermission(menu, route) {
  if (route.path.indexOf('/') !== 0) {
    return menu.indexOf(route.path) !== -1
  }

  return menu.indexOf(route.path.slice(1)) !== -1
}

/**
 * Filter asynchronous routing tables by recursion
 * @param routes asyncRoutes
 * @param roles
 */
export function filterAsyncRoutes(routes, menu) {
  const res = [];

  routes.forEach((route) => {
    const tmp = { ...route };
    if (hasPermission(menu, tmp)) {
      if (tmp.children) {
        tmp.children = filterAsyncRoutes(tmp.children, menu);
      }
      res.push(tmp);
    }
  });

  return res;
}

const state = {
  routes: [],
  addRoutes: [],
};

const mutations = {
  SET_ROUTES: (state, routes) => {
    state.addRoutes = routes;
    state.routes = constantRoutes.concat(routes);
  },
};

const actions = {
  generateRoutes({ commit }, info) {
    return new Promise((resolve) => {
      const { roleId, menu, authMenu } = info;

      /********************  解决动态路由问题开始 ***********************/
      let authAsyncRoutes = [];
      authMenu.forEach((element) => {
        let menuInfo = {};
        if (element.children.length == 0) {
          // 自定义路由
          if (element.flag == "diy") {
            menuInfo = {
              path: element.web_path,
              component: Layout,
              redirect: element.web_path,
              meta: {
                title: element.node_name,
                icon: element.node_icon,
              },
              children: [
                {
                  path: element.web_path,
                  name: element.node_name,
                  component: () => import("@/views/design/index"),
                  meta: {
                    title: element.node_name,
                    icon: element.node_icon,
                    id: element.component.split("_")[1],
                  },
                },
              ],
            };
          } else {
            menuInfo = {
              path: element.web_path,
              component: Layout,
              redirect: element.web_path,
              meta: {
                title: element.node_name,
                icon: element.node_icon,
              },
              children: [
                {
                  path: element.web_path,
                  name: element.node_name,
                  component: (resolve) =>
                    require(["@/views" + ele.web_path], resolve),
                  meta: { title: element.node_name, icon: element.node_icon },
                },
              ],
            };
          }
        } else {
          menuInfo = {
            path: element.web_path,
            component: Layout,
            alwaysShow: true,
            name: element.node_name,
            meta: {
              title: element.node_name,
              icon: element.node_icon,
            },
            children: [],
          };

          element.children.forEach((ele) => {
            menuInfo.children.push({
              path: ele.component,
              component: (resolve) =>
                require(["@/views" + ele.web_path], resolve), // 动态的得用 require
              name: ele.node_name,
              meta: {
                title: ele.node_name,
              },
            });
          });
        }

        authAsyncRoutes.push(menuInfo);
      });
      /********************  解决动态路由问题结束 ***********************/

      let accessedRoutes;
      if (roleId === 1) {
        accessedRoutes = authAsyncRoutes || [];
      } else {
        accessedRoutes = filterAsyncRoutes(authAsyncRoutes, menu);
      }

      commit("SET_ROUTES", accessedRoutes);
      resolve(accessedRoutes);
    });
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
