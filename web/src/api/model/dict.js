import http from "@/utils/request"

export default {
	tree: {
        url: '/dictcate/index',
        name: "获取字典树",
        get: async function(){
            return await http.get(this.url);
        }
    },
    all: {
        url: '/dictcate/all',
        name: "获取所有的字典",
        get: async function(){
            return await http.get(this.url);
        }
    },
    cateInfo: {
        url: '/dict/getDictByCateId',
        name: "根据分类获取字典的信息",
        get: async function(params) {
            return await http.get(this.url, params);
        }
    },
    list: {
        url: '/dict/index',
        name: "获取字典列表",
        get: async function(params){
            return await http.get(this.url, params);
        }
    },
    addCate: {
        url: '/dictcate/add',
        name: "添加字典分类",
        post: async function(params){
            return await http.post(this.url, params);
        }
    },
    editCate: {
        url: '/dictcate/edit',
        name: "编辑字典分类",
        post: async function(params){
            return await http.post(this.url, params);
        }
    },
    delcate: {
        url: '/dictcate/del',
        name: "删除字典分类",
        get: async function(params){
            return await http.get(this.url, params);
        }
    },
    addDict: {
        url: '/dict/add',
        name: "添加字典信息",
        post: async function(params){
            return await http.post(this.url, params);
        }
    },
    editDict: {
        url: '/dict/edit',
        name: "编辑字典信息",
        post: async function(params){
            return await http.post(this.url, params);
        }
    },
    delDict: {
        url: '/dict/del',
        name: "删除字典",
        get: async function(params){
            return await http.get(this.url, params);
        }
    }
}