<?php

namespace app\controller;

use app\BaseController;
use app\service\LoginService;

class Login extends BaseController
{
    public function initialize()
    {
        header("access-control-allow-headers: Accept,Authorization,Cache-Control,Content-Type,DNT,If-Modified-Since,Keep-Alive,Origin,User-Agent,X-Mx-ReqToken,X-Requested-With");
        header("access-control-allow-methods: GET, POST, PUT, DELETE, HEAD, OPTIONS");
        header("access-control-allow-credentials: true");
        header("access-control-allow-origin: *");
    }

    public function doLogin()
    {
        if (request()->isPost()) {

            $param = input('post.');

            $loginService = new LoginService();
            $res = $loginService->login($param);

            return json($res);
        }
    }

    public function getMenu()
    {
        $user = getUserSimpleInfo(getHeaderToken());
        $loginService = new LoginService();
        $res = $loginService->getMenu($user['data']['roleId'], $user['data']['id']);

        return json($res);
    }

    public function loginOut()
    {
        $userInfo = getUserSimpleInfo(getHeaderToken())['data'];

        app('jwt.manager')->destroyToken($userInfo['id'], 'admin');
        return jsonReturn(0, 'success');
    }
}