<?php

namespace app\validate;

use think\Validate;

class RoleValidate extends Validate
{
    protected $rule = [
        'name|角色名' => 'require',
        'status|状态' => 'require',
        'role_node|操作权限' => 'require'
    ];
}