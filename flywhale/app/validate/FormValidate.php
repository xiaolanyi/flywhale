<?php

namespace app\validate;

use think\Validate;

class FormValidate extends Validate
{
    protected $rule = [
        'title|表单名称' => 'require|chs|max:255',
        'table|表名' => 'require|regex: /^[a-z][a-z0-9\_]+$/|max:55',
        'status|状态' => 'require',
    ];
}